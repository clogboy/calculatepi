﻿using System.Windows;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalcPi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        decimal piCalc = 3;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            decimal n = decimal.Parse(tboStappen.Text);
            piCalc = 3;

            //Wisselt tussen methodes wanneer de checkbox Async is ingeschakeld
            if (chbAsync.IsChecked == true)
                //async task. Geeft helaas geen betrouwbare resultaten.
                await calculatePiSegments(n);

            //Lineaire for-loop. Deze is wel betrouwbaar.
            else CalcPi(n);


            lblResult.Content = "Pi = " + piCalc.ToString();
        }

        /// <summary>
        /// voert een seriële berekening uit van Pi, herhaalt volgens het aantal ingevoerde stappen
        /// </summary>
        /// <param name="n"></param>
        private void CalcPi(decimal n)
        {
            for (decimal i = 2; i < n * 4; i++)
            {
                decimal x = (i - 2) * 4 + 2;
                decimal piTemp = 4 / (x * (x + 1) * (x + 2));
                piTemp -= 4 / ((x + 2) * (x + 3) * (x + 4));

                piCalc += piTemp;
            }
        }

        /// <summary>
        /// voert een parallelle berekening uit van Pi, herhaalt volgens het aantal ingevoerde stappen
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        async Task calculatePiSegments(decimal n)
        {
            await Task.Run(() =>
           {
               Parallel.For(2, (int)n * 4, i =>
               {
                   decimal x = (i - 2) * 4 + 2;
                   decimal piTemp = 4 / (x * (x + 1) * (x + 2));
                   piTemp -= 4 / ((x + 2) * (x + 3) * (x + 4));

                   piCalc += piTemp;
               });
           });
        }
    }
}
